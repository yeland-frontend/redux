import React from 'react';

const Profile = ({name, gender, description}) => {
  return (<div>
    <div>
      <label>User Name: </label>
      <span>{name}</span>
    </div>
    <div>
      <label>Gender: </label>
      <span>{gender}</span>
    </div>
    <div>
      <label>Description: </label>
      <span>{description}</span>
    </div>
  </div>)
};

export default Profile;