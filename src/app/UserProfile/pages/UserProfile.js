import React, {Component} from 'react';
import {Link} from "react-router-dom";
import Profile from "../components/Profile";
import {getUserProfile} from "../action/userActions";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";

class UserProfile extends Component {
  constructor(props, context) {
    super(props, context);
    this.postData = this.postData.bind(this);
  }

  componentDidMount() {
    this.props.getUserProfile(this.props.match.params.id);
  }

  render() {
    const {name, gender, description} = this.props.userProfile;

    return (
      <div>
        <h3>User Profile</h3>
        <Profile name={name} gender={gender} description={description} />
        <button onClick={this.postData}>Like</button>
        <p><Link to="/">&lt; Back to home</Link></p>
      </div>
    );
  }

  postData() {
    const request = {userProfileId: 1, userName: "anonymous user"};
    const string = JSON.stringify(request);
    fetch("http://localhost:8080/api/like",
      {method:"POST", body: string, headers: {'Content-Type': 'application/json'}})
      .then();
  }
}

const mapStateToProps = state => ({
  userProfile: state.user.userProfile
});

const mapDispatchToProps = dispatch => bindActionCreators({
  getUserProfile
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(UserProfile);