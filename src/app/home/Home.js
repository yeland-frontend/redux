import React from 'react';
import {Link} from "react-router-dom";

const Home = () => {
  return (
    <div>
      <h3>This is a beautiful home page.</h3>
      <ul>
        <li>
          <Link to='/product-details'>Go to Product Details Page</Link>
        </li>
      </ul>
      <h3>User Profile</h3>
      <ul>
        <li>
          <Link to='/user-profiles/1'>User Profiles 1</Link>
        </li>
        <li>
          <Link to='/user-profiles/2'>User Profiles 2</Link>
        </li>
      </ul>
    </div>
  )
};
export default Home;